//
//  WelcomeViewController.swift
//  iNotebook
//
//  Created by shiela.m.v.mendoza on 08/01/2020.
//  Copyright © 2020 shiela.m.v.mendoza. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = ""
        var charIndex = 0.0
        
        let titleText = "iNotebook"
        for letter in titleText {
            Timer.scheduledTimer(withTimeInterval: 0.1 * charIndex, repeats: false) { (timer) in
                self.titleLabel.text?.append(letter)
            }
            charIndex += 1
        }
        
    }
    
    @IBAction func facebookButtonPressed(_ sender: UIButton) {
        print("Facebook button pressed!")
    }
    
    @IBAction func linkedinButtonPressed(_ sender: UIButton) {
        print("Linkedin button pressed!")
    }
    
    @IBAction func gmailButtonPressed(_ sender: UIButton) {
        print("Gmail button pressed!")
    }
    
    
}
