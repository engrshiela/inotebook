//
//  LoginViewController.swift
//  iNotebook
//
//  Created by shiela.m.v.mendoza on 08/01/2020.
//  Copyright © 2020 shiela.m.v.mendoza. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    
    
    @IBAction func loginPressed(_ sender: UIButton) {
        if let email = emailTextfield.text, let password = passwordTextfield.text {
            Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
              if let e = error {
                  // if possible insert pop up error; check the remarks on the code
                  print(e.localizedDescription)
              } else {
                  // Navigate to the DashboardViewController
                self.performSegue(withIdentifier: Constants.loginSegue, sender: self)
              }
            }
        }
    }
    
}
