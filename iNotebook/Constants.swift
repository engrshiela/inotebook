//
//  Constants.swift
//  iNotebook
//
//  Created by shiela.m.v.mendoza on 09/01/2020.
//  Copyright © 2020 shiela.m.v.mendoza. All rights reserved.
//

struct Constants {
    static let registerSegue = "RegisterToDashboard"
    static let loginSegue = "LoginToDashboard"
    static let addDashboardSegue = "AddDashboard"
}
